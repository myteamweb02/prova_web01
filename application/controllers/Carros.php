<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carros extends CI_Controller {

	private $header = 'partils/header';
	private $footer = 'partils/footer';
	private $data = array();

	public function __construct() {
        parent::__construct();
        
       $this->load->model('carros_model');
    }

	public function index()
	{
		$this->data['carros'] = $this->carros_model->get();
		$this->load->view($this->header);
		$this->load->view('carros/carros_listar', $this->data);
		$this->load->view($this->footer);
	}

	public function cadastrar(){

        $this->data['carros'] = $this->carros_model->get();

		if($this->input->post()){

			$this->carros_model->insert($this->input->post());

			redirect('carros');

		}else{
			$this->load->view($this->header);
			$this->load->view('carros/carros_cadastrar', $this->data);
			$this->load->view($this->footer);
		}
		
    }
    
}
