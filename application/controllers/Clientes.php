<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

	private $header = 'partils/header';
	private $footer = 'partils/footer';
	private $data = array();

	public function __construct() {
        parent::__construct();
        
       $this->load->model('clientes_model');

    }

	public function index()
	{
		$this->data['clientes'] = $this->clientes_model->get();
		$this->load->view($this->header);
		$this->load->view('clientes/clientes_listar', $this->data);
		$this->load->view($this->footer);
	}

	public function cadastrar(){

		if($this->input->post()){

			$this->clientes_model->insert($this->input->post());

			redirect('clientes');

		}else{
			$this->load->view($this->header);
			$this->load->view('clientes/clientes_cadastrar', $this->data);
			$this->load->view($this->footer);
		}
		
	}
}
