<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alugueis extends CI_Controller {

	private $header = 'partils/header';
	private $footer = 'partils/footer';
	private $data = array();

	public function __construct() {
        parent::__construct();
        
       $this->load->model('alugueis_model');
       $this->load->model('clientes_model');
       $this->load->model('Carros_model');
    }

	public function index()
	{
		$this->data['alugueis'] = $this->alugueis_model->get();
		$this->load->view($this->header);
		$this->load->view('alugueis/alugueis_listar', $this->data);
		$this->load->view($this->footer);
	}

	public function cadastrar(){

        $this->data['clientes'] = $this->clientes_model->get();
        $this->data['carros'] = $this->Carros_model->get();

		if($this->input->post()){

			$this->alugueis_model->insert($this->input->post());

			redirect('clientes');

		}else{
			$this->load->view($this->header);
			$this->load->view('alugueis/alugueis_cadastrar', $this->data);
			$this->load->view($this->footer);
		}
		
    }

    public function buscaAlugados(){

        $this->data['clientes'] = $this->clientes_model->get();

        if($this->input->get()){

            $carrosAlugados = $this->alugueis_model->getAlugueisCarros($this->input->get('nome'));

            $this->session->set_flashdata('clientes_results',  $carrosAlugados);
            
            redirect('/alugueis/buscaAlugados');
		}else{
			$this->load->view($this->header);
			$this->load->view('buscas/carros_alugados', $this->data);
			$this->load->view($this->footer);
		}
    }
    
}
