<h2>Lista de Clientes</h2>


<table class="table">
    <thead>
        <tr>
            <th>CPF</th>
            <th>Salario</th>
            <th>Nome</th>
            <th>Idade</th>
            <th>Ação</th>
        </tr>
    </thead>
    <tbody>
        <?php

        foreach ($clientes as $key => $cliente): ?>
        <tr>
            <td><?= $cliente->cpf?></td>
            <td><?= $cliente->nome?></td>
            <td><?= $cliente->salario?></td>
            <td><?= $cliente->idade?></td>
            <td><a href='#'>Deletar</a></td>
        </tr>

        <?php endforeach ?>
    </tbody>
</table>

<div class="sm-w-12 flow-root" style="margin-top: 15px;">
	<a class="button" href="<?= base_url("clientes/cadastrar") ?>">Novo Cliente</a>
</div>