<h2>Adicionar Cliente</h2>



<form method="post" action="<?= base_url("clientes/cadastrar") ?>">
    <div class="m-b-1">
        <label class="input-label" for="cpf">CPF</label>
        <input class="input-text" type="number" name="cpf" id="cpf" required>
    </div>

    <div class="m-b-1">
        <label class="input-label" for="nome">Nome</label>
        <input class="input-text" type="text" name="nome" id="nome" required>
    </div>

    <div class="m-b-1">
        <label class="input-label" for="salario">Salario</label>
        <input class="input-text" type="text" name="salario" id="salario" required>
    </div>

    <div class="m-b-2">
        <label class="input-label" for="idade">Idade</label>
        <input class="input-text" type="number" name="idade" id="idade" required>
    </div>

    <div class="m-b-1">
        <input class="button" type="submit" id="submit" value="Adicionar">
    </div>
</form>