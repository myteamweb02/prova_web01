<h2>Listar Por Nome</h2>

<p>Lista os carros alugados por um cliente a partir do seu nome.</p>

<form method="get" action="<?= base_url("alugueis/buscaAlugados") ?>">
    <div class="m-b-1">
        <datalist id="myList">
            <?php foreach ($clientes as $key => $cliente): ?>
                    <option value='<?= $cliente->nome ?>'></option>
            <?php endforeach?>
        </datalist>

        <input class="input-text inline" placeholder="Digite o nome do cliente" 
            type="text" name="nome" id="nome" list="myList">

        <input class="button inline" type="submit" id="submit" value="Listar">
    </div>
</form>

<table class="table">
    <thead>
        <tr>
            <th>Nome do Cliente</th>
            <th>CPF do Cliente</th>
            <th>Chassi do Carro</th>
            <th>Modelo do Carro</th>
            <th>Ano do Carro</th>
        </tr>
    </thead>
    <tbody>
            
            <?php if($this->session->flashdata('clientes_results')){ ?>
            <?php foreach ($this->session->flashdata('clientes_results') as $key => $cliente_result): ?>
            <tr>
                <td><?= $cliente_result->nome?></td>
                <td><?= $cliente_result->cpf?></td>
                <td><?= $cliente_result->chassiCarro?></td>
                <td><?= $cliente_result->modelo?></td>
                <td><?= $cliente_result->ano?></td>
            </tr>

            <?php endforeach ?>
            <?php } ?>

    </tbody>
</table>