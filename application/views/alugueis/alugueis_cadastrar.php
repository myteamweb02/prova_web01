<h2>Adicionar Aluguel</h2>

<form method="post" action="<?= base_url("alugueis/cadastrar") ?>">
    <div class="m-b-1">
        <label class="input-label" for="cpfCliente">Cliente</label>
        <select class="input-text" name="cpfCliente">
            <?php foreach ($clientes as $key => $cliente): ?>
                    <option value="<?= $cliente->cpf ?>"><?= $cliente->nome ?></option>
            <?php endforeach ?>
        </select>
    </div>

    <div class="m-b-1">
        <label class="input-label" for="chassiCarro">Carro</label>
        <select class="input-text" name="chassiCarro">
            <?php foreach ($carros as $key => $carro): ?>
                    <option value="<?= $carro->chassi ?>"><?= $carro->modelo ?></option>
            <?php endforeach ?>
        </select>
    </div>

    <div class="m-b-1">
        <input class="button" type="submit" id="submit" value="Adicionar">
    </div>
</form>