<h2>Lista de Clientes</h2>

<table class="table">
    <thead>
        <tr>
            <th>CPF do Cliente</th>
            <th>Chassi do Carro</th>
            <th>Data do Aluguel</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($alugueis as $key => $aluguel): ?>
        <tr>
            <td><?= $aluguel->nome?></td>
            <td><?= $aluguel->chassiCarro?></td>
            <td><?= $aluguel->dataAluguel?></td>
        </tr>

        <?php endforeach ?>
    </tbody>
</table>

<div class="sm-w-12 flow-root" style="margin-top: 15px;">
	<a class="button" href="<?= base_url("alugueis/cadastrar") ?>">Novo Aluguel</a>
</div>