<h2>Lista de Carros</h2>

<table class="table">

    <thead>
        <tr>
            <th>Chassi</th>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Ano</th>
            <th>Ação</th>
        </tr>
    </thead>
    <tbody>
        <?php

        foreach ($carros as $key => $carro):  ?>
        <tr>
            <td><?= $carro->chassi ?></td>
            <td><?= $carro->marca ?></td>
            <td><?= $carro->modelo ?></td>
            <td><?= $carro->ano ?></td>
            <td><a href='#'>Deletar</a></td>
        </tr>

        <?php endforeach ?>
    </tbody>
</table>

<div class="sm-w-12 flow-root" style="margin-top: 15px;">
	<a class="button" href="<?= base_url("carros/cadastrar") ?>">Novo Aluguel</a>
</div>