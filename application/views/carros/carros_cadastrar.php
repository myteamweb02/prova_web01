<h2>Adicionar Carro</h2>

<form method="post" action="<?= base_url("carros/cadastrar") ?>">
    <div class="m-b-1">
        <label class="input-label" for="chassi">Chassi</label>
        <input class="input-text" type="text" name="chassi" id="chassi" required>
    </div>

    <div class="m-b-1">
        <label class="input-label" for="marca">Marca</label>
        <input class="input-text" type="text" name="marca" id="marca" required>
    </div>

    <div class="m-b-1">
        <label class="input-label" for="modelo">Modelo</label>
        <input class="input-text" type="text" name="modelo" id="modelo" required>
    </div>

    <div class="m-b-2">
        <label class="input-label" for="ano">Ano de Fabricação</label>
        <input class="input-text" type="date" name="ano" id="ano" required>
    </div>

    <div class="m-b-1">
        <input class="button" type="submit" id="submit" value="Adicionar">
    </div>
</form>