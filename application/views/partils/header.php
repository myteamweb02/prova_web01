<!doctype html>
<html>
<head>
	<link href="<?= base_url("assets/layout.css") ?>" rel="stylesheet">
	<meta content="text/html;charset=utf-8" http-equiv="content-type">
	<meta content="width=device-width,initial-scale=1" name="viewport">
</head>
<body>
	<header>
		<nav class="nav nav-black">
			<div class="content sm-w-12 flex">
				<ul class="nav-list">
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url("clientes") ?>">Cliente</a>
					</li> <!-- .nav-item -->
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url("alugueis") ?>">Aluguel</a>
					</li> <!-- .nav-item -->
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url("carros") ?>">Carro</a>
					</li> <!-- .nav-item -->
				</ul> <!-- .nav-list -->

				<ul class="nav-list m-l-auto">
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url("alugueis/buscaAlugados") ?>">Listar por Nome</a>
					</li> <!-- .nav-item -->
					<li class="nav-item">
						<a class="nav-link" href="#">Listar por Chassi</a>
					</li> <!-- .nav-item -->
					<li class="nav-item">
						<a class="nav-link" href="#">Total em Revisões</a>
					</li> <!-- .nav-item -->
				</ul> <!-- .nav-list -->
			</div> <!-- .content .sm-w-8 .flex -->
		</nav> <!-- .nav .nav-black -->
	</header>

    <div class="content sm-w-12 flow-root">