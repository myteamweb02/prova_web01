<?php

class Clientes_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get(){
        return $this->db->get('cliente')->result();
    }

    public function insert($data){
        $this->db->insert('cliente', $data);
    }

}