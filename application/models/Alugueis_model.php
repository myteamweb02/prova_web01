<?php

class Alugueis_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get(){
        $this->db->select('aluguel.*, cliente.nome');  
        $this->db->join('cliente', 'aluguel.cpfCliente = cliente.cpf');
        return $this->db->get('aluguel')->result();
    }

    public function insert($data){
        $this->db->insert('aluguel', $data);
    }

    public function getAlugueisCarros($nome){
        $this->db->select('aluguel.*, cliente.*, carro.*');  
        $this->db->join('cliente', 'aluguel.cpfCliente = cliente.cpf');
        $this->db->join('carro', 'carro.chassi = aluguel.chassiCarro');
        $this->db->like('cliente.nome',$nome);
        return $this->db->get('aluguel')->result();
    }
    

}