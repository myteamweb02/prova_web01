
<?php

class Carros_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get(){
        return $this->db->get('carro')->result();
    }

    public function insert($data){
        $this->db->insert('carro', $data);
    }

}